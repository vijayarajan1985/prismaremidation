import boto3
import csv
import time
import json
import sys
import re
import ast
from functools import reduce

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

def get_temporary_cross_account_cred(id):
    sts_connection = boto3.client('sts')
    acct_b = sts_connection.assume_role(
    RoleArn="arn:aws:iam::{}:role/Child-Cross-Account-Resourcegroupstaggingapi-Role".format(id),
    RoleSessionName="cross_acct_lambda")            
    ACCESS_KEY = acct_b['Credentials']['AccessKeyId']
    SECRET_KEY = acct_b['Credentials']['SecretAccessKey']
    SESSION_TOKEN = acct_b['Credentials']['SessionToken']
    return(ACCESS_KEY,SECRET_KEY,SESSION_TOKEN)
   
def is_empty_csv(fname):
    with open(fname) as f:
       reader = csv.reader(f)
       for i, _ in enumerate(reader):
            if i:  # found the second row
                return False
    return True

def main(): 
    
    try:
        #Global Variables
        currenttime = '{}'.format(time.strftime("%d-%m-%Y-[%H:%M:%S]"))

        #Initiate CSV File
        cols = ["AccountId", "AccountName", "Region", "ResourceType", "ResourceARN", "Application Tag Value", "Consumer Tag Value", "Costcenter Tag Value", "DataClassification Tag Value", "Division Tag Value", "Environment Tag Value"]
        fname = lambda : "/tmp/Tagging-Compliance-Report-{}.csv".format(currenttime)
        with open(fname(), 'a') as f:
         # using csv.writer method from CSV package
           write = csv.writer(f)
           write.writerow(cols)
           
    except Exception as e:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        filename = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        logger.info("Error: ", e)
        logger.info("Exception type: ", exception_type)
        logger.info("File name: ", filename)
        logger.info("Line number: ", line_number)
        sns_client = boto3.client(service_name='sns',region_name="us-east-1")
        message = {"foo": "bar"}
        response = sns_client.publish(
            TargetArn='arn:aws:sns:us-east-1:<ACCOUNT NO>:Test2',
            Message=json.dumps({'default': json.dumps(message),
                                'email': 'Glue Job encountered an error while generating the Tagging Compliance Report. Please click or visit the link below to navigate to Glue Jobs Console and check for the detailed Error Logs.\n\nError: "{}" \nException type: {} \nLine number: {}  \nURL: https://ap-southeast-2.console.aws.amazon.com/glue/home?region=ap-southeast-2#etl:tab=jobs'.format(e,exception_type,line_number)}),
            Subject='ERROR: Tagging Compliance Report',
            MessageStructure='json'
        )
        sys.exit(1)    

#------------------------------------------------FOR Loop Iterate through exciting account(START)----------------------------------------------------------

    try:
   
        ssm = boto3.client('ssm','ap-southeast-2')
        parameter = ssm.get_parameter(Name='account_ids')
        account_ids = parameter['Parameter']['Value'].split(',')
       
        for id in account_ids:
           
            SessionSecret=get_temporary_cross_account_cred(id)
                                   
            regions = ['eu-north-1', 'ap-south-1', 'eu-west-3', 'eu-west-2', 'eu-west-1', 'ap-northeast-3', 'ap-northeast-2', 'ap-northeast-1', 'sa-east-1', 'ca-central-1', 'ap-southeast-1', 'ap-southeast-2', 'eu-central-1', 'us-east-1', 'us-east-2', 'us-west-1', 'us-west-2']
           
            for region in regions:

                # create service client using the assumed role credentials, e.g. resourcegroupstaggingapi
                resourcegroupstaggingapi_client = boto3.client(
                'resourcegroupstaggingapi',region_name=region,
                aws_access_key_id=SessionSecret[0],
                aws_secret_access_key=SessionSecret[1],
                aws_session_token=SessionSecret[2],)
                region=resourcegroupstaggingapi_client.meta.region_name
               
                # create service client using the assumed role credentials, e.g. IAM
                alias = boto3.client('iam',
                aws_access_key_id=SessionSecret[0],
                aws_secret_access_key=SessionSecret[1],
                aws_session_token=SessionSecret[2]).list_account_aliases()['AccountAliases'][0]

                #To get the list of Complaint tagged resources
                ResourceType=[]
                ResourceList=[]
                Application=[]
                Consumer=[]
                Costcenter=[]
                DataClassification=[]
                Division=[]
                Environment=[]
               
                #Tag-Filters
                filter1={'Key': 'Consumer'}
               
                paginator = resourcegroupstaggingapi_client.get_paginator('get_resources')    
                for each_page in paginator.paginate(TagFilters=[filter1]):
                    for each_item in each_page['ResourceTagMappingList']:
                      ResourceList.append(each_item['ResourceARN'])
                      ResourceARN = each_item['ResourceARN']
                      RegexSearch = re.search('arn:aws:(\w+)', ResourceARN)
                      if RegexSearch:
                        found = RegexSearch.group(1)
                        ResourceType.append(found)                                    
                    for each_item in each_page['ResourceTagMappingList']:
                       Consumer.append(next((item for item in each_item['Tags'] if item["Key"] == "Consumer"), ast.literal_eval("{'Value': 'Tag N/A'}")))
                       Application.append(next((item for item in each_item['Tags'] if item["Key"] == "Application"), ast.literal_eval("{'Value': 'Tag N/A'}")))
                       Costcenter.append(next((item for item in each_item['Tags'] if item["Key"] == "Costcenter"), ast.literal_eval("{'Value': 'Tag N/A'}")))
                       DataClassification.append(next((item for item in each_item['Tags'] if item["Key"] == "DataClassification"), ast.literal_eval("{'Value': 'Tag N/A'}")))
                       Division.append(next((item for item in each_item['Tags'] if item["Key"] == "Division"), ast.literal_eval("{'Value': 'Tag N/A'}")))
                       Environment.append(next((item for item in each_item['Tags'] if item["Key"] == "Environment"), ast.literal_eval("{'Value': 'Tag N/A'}")))      
                Application=[d['Value'] for d in Application if 'Value' in d]
                Consumer=[d['Value'] for d in Consumer if 'Value' in d]
                Costcenter=[d['Value'] for d in Costcenter if 'Value' in d]
                DataClassification=[d['Value'] for d in DataClassification if 'Value' in d]
                Division=[d['Value'] for d in Division if 'Value' in d]
                Environment=[d['Value'] for d in Environment if 'Value' in d]    
                AccountId=list([id]*len(ResourceList))
                Region=list([region]*len(ResourceList))
                AccountName=list([alias]*len(ResourceList))              
                if (len(ResourceList)!=0):
                    with open(fname(), 'a') as f:
                     for w in range(len(ResourceList)):
                     # using csv.writer method from CSV package
                       write = csv.writer(f)
                       write.writerow([AccountId[w],AccountName[w],Region[w],ResourceType[w],ResourceList[w],Application[w],Consumer[w],Costcenter[w],DataClassification[w],Division[w],Environment[w]])
                                   
    except Exception as e:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        filename = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        logger.info("Error: ", e)
        logger.info("Exception type: ", exception_type)
        logger.info("File name: ", filename)
        logger.info("Line number: ", line_number)
        sns_client = boto3.client(service_name='sns',region_name="us-east-1")
        message = {"foo": "bar"}
        response = sns_client.publish(
            TargetArn='arn:aws:sns:us-east-1:<ACCOUNT NO>:Test2',
            Message=json.dumps({'default': json.dumps(message),
                                'email': 'Glue Job encountered an error while generating the Tagging Compliance Report. Please click or visit the link below to navigate to Glue Jobs Console and check for the detailed Error Logs.\n\nError: "{}" \nException type: {} \nLine number: {}  \nURL: https://ap-southeast-2.console.aws.amazon.com/glue/home?region=ap-southeast-2#etl:tab=jobs'.format(e,exception_type,line_number)}),
            Subject='ERROR: Tagging Compliance Report',
            MessageStructure='json'
        )       
        sys.exit(1)
       
#--------------------------------------------------FOR Loop Iterate through exciting Accounts (END)----------------------------------------------------------
         
    try:
        #Check Whether CSV is empty or not ?
        content = is_empty_csv(fname())
        if (content==True):
            raise ValueError('CSV is empty.')
    except(ValueError, IndexError):
        sns_client = boto3.client(service_name='sns',region_name="us-east-1")
        message = {"foo": "bar"}
        response = sns_client.publish(
            TargetArn='arn:aws:sns:us-east-1:<ACCOUNT NO>:Test2',
            Message=json.dumps({'default': json.dumps(message),
                                'email': 'Glue Job encountered an error while generating the Tagging Compliance Report: "Could not complete request. Tagging Compliance Report is empty". Please click or visit the link below to navigate to Glue Jobs Console and check for the Detailed Error Logs.\n\nhttps://ap-southeast-2.console.aws.amazon.com/glue/home?region=ap-southeast-2#etl:tab=jobs'}),
            Subject='ERROR: Tagging Compliance Report',
            MessageStructure='json'
        )    
        exit('Could not complete request. Tagging Compliance Report is empty')

    try:            
        #Copy CSV File to S3    
        s3 = boto3.resource('s3')
        s3.meta.client.upload_file('{}'.format(fname()), 'tagging-compliance-report', '{}'.format(fname()))
       
        #Send Notification to Stakeholders after publishing the Report
        sns_client = boto3.client(service_name='sns',region_name="us-east-1")
        message = {"foo": "bar"}
        response = sns_client.publish(
            TargetArn='arn:aws:sns:us-east-1:<ACCOUNT NO>:Test2',
            Message=json.dumps({'default': json.dumps(message),
                                'email': 'Tagging Compliance Report has been generated. Click the following link to download the report: https://s3.console.aws.amazon.com/s3/object/tagging-compliance-report?region=ap-southeast-2&prefix={}'.format(fname())}),
            Subject='Tagging Compliance Report has been Generated - {} UTC'.format(currenttime),
            MessageStructure='json'
        )  
        print("Tagging Compliance Report has been generated!")

    except Exception as e:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        filename = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        logger.info("Error: ", e)
        logger.info("Exception type: ", exception_type)
        logger.info("File name: ", filename)
        logger.info("Line number: ", line_number)
        sns_client = boto3.client(service_name='sns',region_name="us-east-1")
        message = {"foo": "bar"}
        response = sns_client.publish(
            TargetArn='arn:aws:sns:us-east-1:<ACCOUNT NO>:Test2',
            Message=json.dumps({'default': json.dumps(message),
                                'email': 'Glue Job encountered an error while generating the Tagging Compliance Report. Please click or visit the link below to navigate to Glue Jobs Console and check for the detailed Error Logs.\n\nError: "{}" \nException type: {} \nLine number: {}  \nURL: https://ap-southeast-2.console.aws.amazon.com/glue/home?region=ap-southeast-2#etl:tab=jobs'.format(e,exception_type,line_number)}),
            Subject='ERROR: Tagging Compliance Report',
            MessageStructure='json'
        )       
        sys.exit(1)

    
if __name__=="__main__":                                                                                                  
    main()     